# vscode-setting

git clone  
clone into your home directory.

## Install setting (for Mac)

setting directory
 > ~/Library/Application\ Support/Code/User

```
cd ~/Library/Application\ Support/Code/
rm -rf User
ln -s ~/vscode-setting/User User
```

## Install setting (for Win)

```
cd "%appdata%\Code"
cmd /c mklink /D User "%homepath%\vscode-setting\User"
```


## Install Extensions

./extensions.txt に記載されているプラグインをインストールします。

```
./installExtensions.sh
```

VSCODEにインストールされているプラグインを、./extensions.txtに書き出します。

```
./makeExtensions.sh
```




